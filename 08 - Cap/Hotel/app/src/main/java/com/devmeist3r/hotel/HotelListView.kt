package com.devmeist3r.hotel

interface HotelListView {
  fun showHotels(hotels: List<Hotel>)
  fun showHotelDetails(hotel: Hotel)
  fun showDeleteMode()
  fun hideDeleteMode()
  fun showSelectedHostels(hotels: List<Hotel>)
  fun updateSelectionCountText(count: Int)
}
